@section('conteudo')

	<div class="colunas">
		<div class="coluna coluna-1-2">
			<div class="pad">
				<h1>Contato e Localização</h1>
				
				<div class="contato-telefone">
					{{ $texto->telefone }}
				</div>
				<div class="contato-endereco">
					{{ $texto->endereco }}
				</div>
				<div class="contato-detalhes">
					{{ $texto->endereco_detalhes }}
				</div>
				
				@if(Session::has('envio'))
					<div class="sucesso">
						SUA MENSAGEM FOI ENVIADA.
						<br><br>
						AGRADECEMOS SEU CONTATO.<br>
						RETORNAREMOS EM BREVE.
					</div>
				@else

					<form action="{{URL::route('contato.enviar')}}" method="post" id="form-contato">
						<input type="text" name="nome" placeholder="nome" required id="contato-nome">
						<input type="email" name="email" placeholder="e-mail" required id="contato-email">
						<input type="text" name="telefone" placeholder="telefone">
						<textarea name="mensagem" placeholder="mensagem" required id="contato-mensagem"></textarea>
						<input type="submit" value="ENVIAR">
					</form>
					
				@endif

				<div id="trabalhe-conosco">
					<h2>Trabalhe conosco</h2>
					<a href="contato#shadow-envie-curriculo" title="Envie Seu Currículo">ENVIE SEU CURRÍCULO</a>
				</div>
			</div>
		</div>
		<div class="coluna coluna-1-2 maps">
			<div class="pad">
				{{ Tools::viewGMaps($texto->google_maps, '100%', '600px') }}
			</div>
		</div>
	</div>

@stop